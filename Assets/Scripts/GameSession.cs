﻿using TMPro;
using UnityEngine;

public class GameSession : MonoBehaviour
{
    // Parameters
    [Range(0.1f, 10f)][SerializeField] private float gameSpeed = 1f;
    [SerializeField] private int pointsPerBlock = 10;
    [SerializeField] private bool isAutoPlay;
    
    // State variables
    [SerializeField] private int currentScore = 0;
    
    // Scene GameObject
    [SerializeField] private TextMeshProUGUI text;

    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameSession>().Length;
        if (gameStatusCount > 1)
        {
            GameObject o;
            (o = gameObject).SetActive(false);
            Destroy(o);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        text.text = currentScore.ToString();
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        Time.timeScale = gameSpeed;
    }

    public void AddScore()
    {
        currentScore += pointsPerBlock;
        text.text = currentScore.ToString();
    }

    public void DestroySession()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return isAutoPlay;
    }
}
