﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    // config params
    [SerializeField] private Transform paddleTransform;
    [SerializeField] private float xPush = 2f;
    [SerializeField] private float yPush = 15f;
    [SerializeField] private AudioClip[] ballSounds;
    [SerializeField] private float randomFactor;

    // Cached component references
    private Rigidbody2D rb;
    private AudioSource audioSource;
    
    // State
    private Vector2 paddleToBallVector;
    private bool hasStarted;

    void Start()
    {
        paddleToBallVector = transform.position - paddleTransform.position;
        rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        if (!hasStarted)
        {
            LockBallToPaddle();
            LaunchOnMouseClick();    
        }
    }

    private void LaunchOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rb.velocity = new Vector2(xPush, yPush);
            hasStarted = true;
        }
    }

    private void LockBallToPaddle()
    {
        var paddlePos = paddleTransform.position;
        transform.position = new Vector2(paddlePos.x, paddlePos.y) + paddleToBallVector;
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (hasStarted)
        {
            BallSound();
            BallRandom();
        }
            
    }

    /// <summary>
    /// Add random to ball to avoid boring loops
    /// </summary>
    private void BallRandom()
    {
        var randomVelocity = new Vector2(
            Random.Range(0, randomFactor), 
            Random.Range(0, randomFactor));

        // To avoid speed decease due to random velocity
        if (Vector2.Dot(rb.velocity, randomVelocity) < 0)
            randomVelocity = -randomVelocity;
        
        rb.velocity += randomVelocity;
    }

    private void BallSound()
    {
        var clip = ballSounds[Random.Range(0, ballSounds.Length)];
        audioSource.PlayOneShot(clip);
    }
}
