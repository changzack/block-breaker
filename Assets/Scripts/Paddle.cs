﻿using System;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    // Parameters
    [SerializeField] private float screenWidthInUnits = 16f;
    [SerializeField] private float minX = 1f;
    [SerializeField] private float maxX = 15f;
    
    // Cached references
    private GameSession gameSession;
    private Ball ball;

    private void Start()
    {
        gameSession = FindObjectOfType<GameSession>();
        ball = FindObjectOfType<Ball>();
    }

    void Update()
    {
        Transform tran = transform;
        tran.position = new Vector2(GetX(), tran.position.y);
    }

    private float GetX()
    {
        if (gameSession.IsAutoPlayEnabled())
        {
            return ball.transform.position.x;
        }

        var newX = Input.mousePosition.x / Screen.width * screenWidthInUnits;
        return Mathf.Clamp(newX, minX, maxX);
    }
}
