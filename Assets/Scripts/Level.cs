﻿using UnityEngine;

public class Level : MonoBehaviour
{
    // Parameters
    private int numOfBlocks;
    
    // Cache references
    private SceneLoader sceneLoader;

    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    }

    public void AddBlockCount()
    {
        numOfBlocks++;
    }

    public void BlockDestroy()
    {
        numOfBlocks--;
        
        if (numOfBlocks <= 0)
            sceneLoader.LoadNextScene();
    }
}
