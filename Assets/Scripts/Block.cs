﻿using UnityEngine;

public class Block : MonoBehaviour
{
    // Config params
    [SerializeField] private AudioClip breakSound;
    [SerializeField] private GameObject blockDestroyVfx;
    [SerializeField] private Sprite[] damagedSprites;

    // Cached references
    private Level level;
    
    // State variables
    private int hitTimes;
 
    private void Start()
    {
        CountBreakableBlock();
    }

    private void CountBreakableBlock()
    {
        if (CompareTag("Breakable"))
        {
            level = FindObjectOfType<Level>();
            level.AddBlockCount();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(CompareTag("Breakable"))
            HandleHit();
    }

    private void HandleHit()
    {
        hitTimes++;
        var maxHits = damagedSprites.Length + 1;
        
        if (hitTimes >= maxHits)
        {
            DestroyBlock();
        }
        else
        {
            ShowNextHitSprite();
        }
    }

    private void ShowNextHitSprite()
    {
        if (damagedSprites != null)
        {
            var spriteIndex = hitTimes - 1;
            GetComponent<SpriteRenderer>().sprite = damagedSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Empty damagedSprite array in: " + name);
        }
        
    }

    private void DestroyBlock()
    {
        FindObjectOfType<GameSession>().AddScore();
        level.BlockDestroy();
        DestroyEffects();
        Destroy(gameObject);
    }

    private void DestroyEffects()
    {
        if (Camera.main != null)
            AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position);

        var tran = transform;
        var vfx = Instantiate(blockDestroyVfx, tran.position, tran.rotation);
        Destroy(vfx, 1f);
    }
}
